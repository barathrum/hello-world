FROM openjdk:11
COPY ./target/hello-world.jar /var/tmp
WORKDIR /var/tmp
RUN sh -c 'touch hello-world.jar'
ENTRYPOINT ["java","-jar","hello-world.jar"]