package com.example.helloworld;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@Slf4j
@CrossOrigin(origins = "*")
public class HelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldApplication.class, args);
	}

	@GetMapping("/hello")
	public String helloWorld() {
		String hello = "Hello world from  " + Thread.currentThread().getName();
		log.info("{}", hello);
		return hello;
	}

	@GetMapping("/")
	public String greetings() {
		log.info("Hello world from Java thread - " + Thread.currentThread().getName());
		return "Hello world from Java thread - " + Thread.currentThread().getName();
	}
}
